package com.github.danielkullmann.examples.mono;

import junit.framework.TestCase;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class ReduceTest extends TestCase{

  private Flux<Integer> integerFluxWithEmpties() {
    return integerFlux().flatMap((number) -> {
      if (number % 5 == 0) {
        return Mono.empty();
      }
      return Mono.just(number);
    });
  }
  
  private Flux<Integer> integerFlux() {
    Integer[] ints = new Integer[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    return Flux.fromArray(ints);
  }

  public void testReduceWithEmptyValues() {
    Mono<Integer> reduced = integerFluxWithEmpties().reduce(0, (sum, number) -> sum+number);
    assertEquals(reduced.block().intValue(), 40);
  }

  public void testReduce() {
    Mono<Integer> reduced = integerFlux().reduce(0, (sum, number) -> sum+number);
    assertEquals(reduced.block().intValue(), 55);
  }
}
