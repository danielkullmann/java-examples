package com.github.danielkullmann.examples.mono;

import junit.framework.TestCase;
import reactor.core.publisher.Mono;

public class MonoTest extends TestCase {

  public Mono<String> stringOrEmpty(boolean empty) {
    if (empty) {
      return Mono.empty();
    } else {
      return Mono.just("hello world");
    }
  }
  
  public void testEmpty() {
    Mono<String> value = stringOrEmpty(false);
    value = value.map((v) -> v + "!");
    assertEquals(value.block(), "hello world!");
    
    value = stringOrEmpty(true);
    value = value.map((v) -> v + "!");
    assertEquals(value.block(), null);
  }

}

