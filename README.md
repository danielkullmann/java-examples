# Java Examples

This is supposed to be a collection of Java snippets for certain tasks.

The raison d'etre is that I always have to lookup how to do simple tasks using Reactors Mono<T> and Flux<T> types; somehow
it is not obvious for me how to use them.

For now, everything is in the `src/test` folder.
